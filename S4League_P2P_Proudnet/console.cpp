#include "stdafx.h"


// Initialize global console object
CConsole g_Console;

CConsole::CConsole()
:
m_Lock(false)
{
#ifdef __CONSOLE_SHOW
    int hConHandle = 0;
    HANDLE lStdHandle = 0;
    FILE *CPacket = 0;
 
    // Allocate a console
    AllocConsole();
 
    // redirect unbuffered STDOUT to the console
    lStdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    hConHandle = _open_osfhandle(PtrToUlong(lStdHandle), _O_TEXT);
    CPacket = _fdopen(hConHandle, "w");
    *stdout = *CPacket;
    setvbuf(stdout, NULL, _IONBF, 0);


	m_hConsole = lStdHandle;

	// Make sure everything is white!
	adjustColor(C_WHITE + C_LIGHT);
#endif

	// Init mutex
	InitializeCriticalSection(&m_mMsg);


	// Obligatory 'Blizzard is a homo!' message!
	__LOG_DBG(C_LIGHTBLUE, "INFO", "Blizzard is a homo! (Program runs in debug mode.)");
}

CConsole::~CConsole()
{
	// Destroy mutex
	DeleteCriticalSection(&m_mMsg);
}

// Common console msg
void CConsole::log(BYTE bColor, const char* pcSys, const char* pcFmt, ...)
{
	EnterCriticalSection(&m_mMsg);

#ifdef __CONSOLE_LOG
	// Console.log logfile
	if(fopen_s(&m_pConsoleLogfile, "./Log/console.log", "a") != 0)
	{
		printf("Couldn't open console logfile!\n");
		m_pConsoleLogfile = NULL;
	}

	// Open secondary logfile
	sprintf_s(m_pcLogfile, sizeof(m_pcLogfile), "./Log/sys/%s.log", pcSys);
	if(fopen_s(&m_pLogfile, m_pcLogfile, "a") != 0)
	{
		printf("Couldn't open sys logfile '%s'!\n", m_pcLogfile);
		m_pLogfile = NULL;
	}
#endif


	// Clock
	GetSystemTime(&m_Time);

#ifdef __CONSOLE_LOG
	// Date
	if(m_pConsoleLogfile != NULL)
		fprintf(m_pConsoleLogfile, "[%02d/%02d/%04d ",
				m_Time.wDay,
				m_Time.wMonth,
				m_Time.wYear);

	if(m_pLogfile != NULL)
		fprintf(m_pLogfile, "[%02d/%02d/%04d ",
				m_Time.wDay,
				m_Time.wMonth,
				m_Time.wYear);
#endif

#ifdef __CONSOLE_SHOW
	// Time
	printf("[%02d:%02d:%02d] | ",
		   m_Time.wHour,
		   m_Time.wMinute,
		   m_Time.wSecond);
#endif


#ifdef __CONSOLE_LOG
	if(m_pConsoleLogfile != NULL)
		fprintf(m_pConsoleLogfile, "%02d:%02d:%02d] | ",
				m_Time.wHour,
				m_Time.wMinute,
				m_Time.wSecond);

	if(m_pLogfile != NULL)
		fprintf(m_pLogfile, "%02d:%02d:%02d] | ",
				m_Time.wHour,
				m_Time.wMinute,
				m_Time.wSecond);
#endif

	
	// Write sys into str
	sprintf_s(m_pcMsg, "[%s]", pcSys);
	sprintf_s(m_pcMsg, "%-13s", m_pcMsg);

#ifdef __CONSOLE_SHOW
	// Print the color prefix
	adjustColor(bColor);
	printf("%s", m_pcMsg);
#endif

#ifdef __CONSOLE_LOG
	if(m_pConsoleLogfile != NULL)
		fprintf(m_pConsoleLogfile, "%s", m_pcMsg);

	if(m_pLogfile != NULL)
		fprintf(m_pLogfile, "%s", m_pcMsg);
#endif

#ifdef __CONSOLE_SHOW
	adjustColor(C_WHITE + C_LIGHT);
#endif

	
	// Argument list
	va_list args;
	// Write into it
	va_start(args, pcFmt);
	// Print them into the msg
	vsnprintf_s(m_pcMsg, sizeof(m_pcMsg), sizeof(m_pcMsg), pcFmt, args);
	// End the usage of the args
	va_end(args);

#ifdef __CONSOLE_SHOW
	// Print the actual string
	printf("| %s\n", m_pcMsg);
#endif

#ifdef __CONSOLE_LOG
	if(m_pConsoleLogfile != NULL)
		fprintf(m_pConsoleLogfile, "| %s\n", m_pcMsg);

	if(m_pLogfile != NULL)
		fprintf(m_pLogfile, "| %s\n", m_pcMsg);

	if(m_pConsoleLogfile != NULL)
		fclose(m_pConsoleLogfile);

	if(m_pLogfile != NULL)
		fclose(m_pLogfile);
#endif


	LeaveCriticalSection(&m_mMsg);
}

void CConsole::adjustColor(BYTE bColor)
{
#ifdef __CONSOLE_SHOW
	SetConsoleTextAttribute(m_hConsole, (WORD)bColor);
#endif
}