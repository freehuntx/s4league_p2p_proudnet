#pragma once

class CP2P
{
public:
	CP2P(void);
	~CP2P(void);
	
	void WSAAttach( );
	void WSADetach( );
private:
	CVtableDetour* h_WSASendTo;
	CVtableDetour* h_WSARecvFrom;
};

