#pragma once
// Enums
enum colors
{
	C_BLACK = 0x00,
	C_BLUE,
	C_GREEN,
	C_LIGHTBLUE,
	C_RED,
	C_VIOLET,
	C_YELLOW,
	C_WHITE,
	C_GRAY,

	C_LIGHT = 0x08,

	// DX colors from here on
	C_LIGHTGREY = 0x08,
	C_ORANGE,
	C_CYAN,
	C_PINK,
	C_LIGHTGREEN,
	C_TRANSPARENT
};

enum direction
{
	DIR_IN = 0,
	DIR_OUT = 1
};

#define __MAGIC 0x5713
#define __P2PMAGIC1 0xABCD
#define __P2PMAGIC2 0xABCE
#define __PROUDHEADERSIZE 16


//----------------------------------------------------

// Console and debug stuff
#define __CONSOLE_SHOW
//===================================================================================
// Regarding comments! USEFUL AS HELL!!! (better debug management)
#define __COMMENT __SLASH(/)
#define __SLASH(s) /##s

// Create the better debug preprocessor define
#ifdef _DEBUG
	#define __DEBUG _DEBUG
#endif

// Debug defines
#ifdef __DEBUG
	// Not do anything when in debug
	#define __DEBUG_ONLY

	#define __ABORT __LOG_DBG_INFO abort()
#else
	// Comment out if not in debug
	#define __DEBUG_ONLY __COMMENT
	#define __ABORT exit(1)
#endif

// Fast asserting
#define __ASSERT __DEBUG_ONLY assert

// Fast logging
#define __LOG g_Console.log
#define __LOG_DBG __DEBUG_ONLY g_Console.log
#define __LOG_DBG_INFO __DEBUG_ONLY g_Console.log(C_YELLOW, "DEBUG", "FILE: "__FILE__" | LINE: %d", __LINE__);
//===================================================================================

#define LOG( fmt, ... ) \
{ \
	printf_s( fmt "\n", __VA_ARGS__ ); \
}

#define LOG_LINE( fmt, ... ) \
{ \
	printf_s (fmt, __VA_ARGS__ ); \
}