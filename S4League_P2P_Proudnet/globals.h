#pragma once

extern CConsole			g_Console;
extern CTools			g_Tools;
extern CSockMgr			g_SockMgr;
extern CP2P*			g_P2P;
extern CP2PHandler		g_P2PHandler;