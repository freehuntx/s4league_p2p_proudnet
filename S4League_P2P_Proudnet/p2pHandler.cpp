#include "stdafx.h"
CP2PHandler g_P2PHandler;


CP2PHandler::CP2PHandler(void)
{

}


CP2PHandler::~CP2PHandler(void)
{

}

void CP2PHandler::handlePacket( CProudpacket_P2P* packet, BYTE direction )
{
	//Lets loop thru every body packet
	for (std::vector<CPacket*>::iterator it = packet->bodys.begin() ; it != packet->bodys.end(); ++it)
	{
		switch( (*it)->readByte() )
		{
		case 0x01: //Plain Word RMI
			switch( (*it)->readWord() )
			{
			case 0x4E32: //Chat ID
				DWORD chatType = (*it)->readDword();
				DWORD playerID = (*it)->readDword();
				(*it)->skip( 11 ); //Some 00 bytes.
				BYTE stringLenPrefix = (*it)->readByte();
				BYTE stringLen = 0;
				if( stringLenPrefix == 1 )
					stringLen = (*it)->readByte();
				else if( stringLenPrefix == 2 )
					stringLen = (*it)->readWord();
				else if( stringLenPrefix == 4 )
					stringLen = (*it)->readDword();

				std::string message = (*it)->readString( stringLen );

				if( direction == DIR_OUT )
					__LOG(C_YELLOW, "Send", "[Chat] PlayerID: %d Message: %s", playerID, message.c_str() );
				else if( direction == DIR_IN )
					__LOG(C_LIGHTBLUE, "Recv", "[Chat] PlayerID: %d Message: %s", playerID, message.c_str() );

				break;
			}
			break;
		}
	}

}
